package com.asteci.demo.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.asteci.demo.model.catalog.GeneralStatus;
import com.asteci.demo.model.entity.ComputerEntity;
import com.asteci.demo.model.entity.OfficeEntity;
import com.asteci.demo.model.entity.OperatorEntity;
import com.asteci.demo.model.entity.ReportEntity;
import com.asteci.demo.service.ReportService;

@RestController
public class ReportController {
	
	@Autowired
	private ReportService service;
	
	@GetMapping("/alive")
	public String alive() {
		return "Ok";
	}
	
	
	@GetMapping("/insert/{reportId}")
	public String insert(@PathVariable String reportId) throws Exception {
		
		// Caso de prueba inicial para validar el insert. Posteriormente debe enviarse como POST desde el cliente
		OfficeEntity office = new OfficeEntity();
		office.setName("Name");
		office.setAddress("Address");
		
		ComputerEntity computer = new ComputerEntity();
		computer.setModel("Model");
		computer.setSerialNumber("Serial");
		computer.setOffice(office);
		
		OperatorEntity operator = new OperatorEntity();
		operator.setUserNumber(1111);
		operator.setOffice(office);
		operator.setName("Name");
		
		
		ReportEntity report = new ReportEntity();
		report.setOffice(office);
		report.setComputer(computer);
		report.setOperator(operator);
		report.setBornDate(LocalDateTime.now());
		report.setStatus(GeneralStatus.OPENED.getValue());
		
		
		service.insert(report);
		
		return "Ok";
	}
	
	
	@GetMapping("/find")
	public Iterable<ReportEntity> findAllReports(){
		return service.findAllReports();
	}
	
	
	@GetMapping("/find/{reportId}")
	public ReportEntity find(@PathVariable Integer reportId){
		Optional<ReportEntity> op = service.findByReportId(reportId);
		if(op.isPresent()) {
			return op.get();
		}
		return null;
	}

}
