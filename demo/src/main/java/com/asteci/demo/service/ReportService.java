package com.asteci.demo.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asteci.demo.model.entity.ReportEntity;
import com.asteci.demo.model.repository.ComputerRepository;
import com.asteci.demo.model.repository.OfficeRepository;
import com.asteci.demo.model.repository.OperatorRepository;
import com.asteci.demo.model.repository.ReportRepository;

@Service
public class ReportService {
	
	@Autowired
	private ReportRepository reportRepository;
	
	@Autowired
	private OfficeRepository officeRepository;
	
	@Autowired
	private ComputerRepository computerRepository;
	
	@Autowired
	private OperatorRepository operatorRepository;
	
	public boolean insert(ReportEntity report) throws Exception{
		officeRepository.save(report.getOffice());
		computerRepository.save(report.getComputer());
		operatorRepository.save(report.getOperator());
		reportRepository.save(report);
		return true;
	}
	
	public Iterable<ReportEntity> findAllReports(){
		return reportRepository.findAll();
	}
	
	public Optional<ReportEntity> findByReportId(Integer id){
		return reportRepository.findById(id);
	}

}
