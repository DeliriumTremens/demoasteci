package com.asteci.demo.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="Report")
@Data
public class ReportEntity {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer id;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id", nullable = false, insertable = false, updatable = false)
	private ComputerEntity computer;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id", nullable = false, insertable = false, updatable = false)
	private OfficeEntity office;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id", nullable = false, insertable = false, updatable = false)
	private OperatorEntity operator;
	@Column(nullable=false) 
	private String description;
	@Column(nullable=false) 
	private LocalDateTime bornDate;
	@Column(nullable=false) 
	private Integer status;
	
	
	
	

}
