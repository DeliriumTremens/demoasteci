package com.asteci.demo.model.catalog;

public enum GeneralStatus {
	
	OPENED(1),
	IN_PROGRESS(2),
	CLOSED(3);
	  
	
  private GeneralStatus(Integer value ) {
    this.value = value;
  }
  
  private Integer value;

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

}
