package com.asteci.demo.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.asteci.demo.model.entity.ReportEntity;

@Repository
public interface ReportRepository extends CrudRepository<ReportEntity, Integer>{

}
