package com.asteci.demo.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.asteci.demo.model.entity.OfficeEntity;

@Repository
public interface OfficeRepository extends CrudRepository<OfficeEntity, Integer>{

}
